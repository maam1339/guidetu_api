require 'rails_helper'

RSpec.describe MainAccount, type: :model do
  let :main_account do
    build(:main_account)
  end

  subject { main_account }

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should be_valid }
  end

  context 'associations' do
    it 'has one super_user' do
      should(
        have_one(:super_user)
          .with_foreign_key('account_id')
          .dependent(:restrict_with_exception)
      )
    end

    it 'have many super_admins' do
      should(
        have_many(:super_admins)
          .class_name('Supers::Admin')
          .with_foreign_key('account_id')
          .dependent(:destroy)
      )
    end
  end
end