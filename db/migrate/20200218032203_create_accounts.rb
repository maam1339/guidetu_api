class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.string :type
      t.string :name
      t.references :account, foreign_key: true, index: true

      t.timestamps
    end
  end
end
