FactoryBot.define do
  factory :account do
    type { "" }
    name { Faker::Company.name }
    account_id { nil }
  end
end
