FactoryBot.define do
  factory :dealer do
    name { Faker::Company.name }
    description { Faker::Company.catch_phrase }
  end
end
