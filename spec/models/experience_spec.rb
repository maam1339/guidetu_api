require 'rails_helper'

RSpec.describe Experience, type: :model do
  let :experience do
    build(:experience)
  end

  subject { experience }

  it { should respond_to(:title) }
  it { should respond_to(:description) }

  context 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should be_valid }
  end

  context 'associations' do
    it { should have_and_belong_to_many(:products) }
  end
end
