class Dealer < ApplicationRecord
  # => Validations
  validates_presence_of :name, :description

  # => Attaches
  has_one_attached :logo
  has_many_attached :images
end
