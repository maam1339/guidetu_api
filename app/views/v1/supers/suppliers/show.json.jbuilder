json.supplier do
  json.partial! 'supplier', supplier: @supplier
end