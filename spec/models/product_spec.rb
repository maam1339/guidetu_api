require 'rails_helper'

RSpec.describe Product, type: :model do
  let :product do
    build(:product)
  end

  subject { product }

  it { should respond_to(:title) }
  it { should respond_to(:description) }
  it { should respond_to(:price) }

  context 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:price) }
    it { should be_valid }
  end

  context 'associations' do
    it { should belong_to(:entity).optional }
    it { should have_and_belong_to_many(:experiences) }
  end
end
