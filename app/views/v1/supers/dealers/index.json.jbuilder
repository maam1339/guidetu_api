json.array! @dealers do |dealer|
  json.partial! 'dealer', dealer: dealer
end