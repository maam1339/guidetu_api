module V1
  module Supers
    class SuppliersController < ApplicationController
      before_action :authenticate!
      before_action :set_supplier, only: %i(show update destroy)

      def index
        @suppliers = Supplier.all
      end

      def create
        @supplier = Supplier.new supplier_params

        if @supplier.save
          render :show, status: :created
        else
          render json: @supplier.errors, status: :unprocessable_entity
        end
      end

      def show; end

      def update
        if @supplier.update supplier_params
          render :show
        else
          render json: @supplier.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @supplier.destroy
      end

      private

      def set_supplier
        @supplier = Supplier.find params.fetch(:id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def supplier_params
        params.require(:supplier).permit(
          :name,
          :description,
          :logo,
          images: [],
          products_attributes: [
            :title,
            :description,
            :price,
            images: []
          ]
        )
      end
    end
  end
end