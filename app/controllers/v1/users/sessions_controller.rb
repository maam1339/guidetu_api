module V1
  module Users
    class SessionsController < ApplicationController
      before_action :authenticable_resource, only: %i(create)
      before_action :authenticate!, only: %i(show destroy)
      before_action(
        :set_user,
        :found,
        :access_locked,
        :valid_password,
        only: %i(create)
      )

      def create
        sign_in @user
        
        render :show, status: :created
      end

      def show
        @user = current_user

        render :show
      end

      def destroy
        current_user.update(authentication_token: nil)

        render(
          json: { message: I18n.t('devise.sessions.signed_out') },
          status: :ok
        )
      end

      private

      def set_user
        @user = resource.find_by email: user_params.fetch(:email)
      rescue ActionController::ParameterMissing
        head :bad_request
      end

      def user_params
        params.require(:user).permit(:email, :password, :authentication_token)
      end

      # => user status verifications
      # that return responces agree
      # with some existace data
      #
      def found
        unless @user
          render(
            json: { error: I18n.t('devise.failure.not_found_in_database') },
            status: :not_found
          )
        end
      end

      # => *
      def access_locked
        if @user.access_locked?
          return render(
            json: { error: I18n.t('devise.failure.locked') },
            status: :locked
          )
        end
      end

      # => *
      def valid_password
        unless @user.valid_password? user_params.fetch(:password)
          @user.lock_if_attempts_exed

          return render(
            json: { error: I18n.t('devise.failure.invalid') },
            status: :unauthorized
          )
        end
      end
    end
  end
end