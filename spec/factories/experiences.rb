FactoryBot.define do
  factory :experience do
    title { Faker::Food.dish }
    description { Faker::Food.description }
  end
end
