json.array! @sponsors do |sponsor|
  json.partial! 'sponsor', sponsor: sponsor
end