module V1
  module Supers
    class SponsorsController < ApplicationController
      before_action :authenticate!
      before_action :set_sponsor, only: %i(show update destroy)

      def index
        @sponsors = Sponsor.all
      end

      def create
        @sponsor = Sponsor.new sponsor_params

        if @sponsor.save
          render :show, status: :created
        else
          render json: @sponsor.errors, status: :unprocessable_entity
        end
      end

      def show; end

      def update
        if @sponsor.update sponsor_params
          render :show
        else
          render json: @sponsor.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @sponsor.destroy
      end

      private

      def set_sponsor
        @sponsor = Sponsor.find params.fetch(:id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def sponsor_params
        params.require(:sponsor).permit(
          :name,
          :description,
          :logo,
          images: [],
          products_attributes: [
            :title,
            :description,
            :price,
            images: []
          ]
        )
      end
    end
  end
end