json.array! @suppliers do |supplier|
  json.partial! 'supplier', supplier: supplier
end