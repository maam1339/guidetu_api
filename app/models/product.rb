class Product < ApplicationRecord
  # => Associations
  belongs_to :entity, polymorphic: true, optional: true
  has_and_belongs_to_many :experiences

  # => Validations
  validates_presence_of :title, :description, :price

  # => Attaches
  has_many_attached :images
end
