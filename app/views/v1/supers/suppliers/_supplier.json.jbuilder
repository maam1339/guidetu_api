json.call(
  supplier,
  :id,
  :name,
  :description,
  :created_at,
  :updated_at,
  :products
)

if supplier.logo.attached?
  json.logo do
    json.partial! 'v1/shared/image', image: supplier.logo
  end
end

json.images do
  json.array! supplier.images do |image|
    json.partial! 'v1/shared/image', image: image
  end
end