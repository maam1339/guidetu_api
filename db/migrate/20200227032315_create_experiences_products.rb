class CreateExperiencesProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :experiences_products do |t|
      t.references :experience, null: false, index: true
      t.references :product, null: false, index: true
    end
  end
end
