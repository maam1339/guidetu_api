require 'rails_helper'

RSpec.describe SuperUser, type: :model do
  let :account do
    create(:main_account)
  end

  let :super_user do
    account.build_super_user attributes_for(:super_user)
  end

  subject { super_user }

  it { should respond_to(:first_name) }
  it { should respond_to(:last_name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }

  context 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password) }
    it { should be_valid }
  end

  context 'associations' do
    it { should belong_to(:account).class_name('MainAccount') }
  end
end
