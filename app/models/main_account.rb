class MainAccount < Account
  # => Associations
  has_one(
    :super_user,
    foreign_key: :account_id,
    dependent: :restrict_with_exception
  )
  has_many(
    :super_admins,
    class_name: 'Supers::Admin',
    foreign_key: :account_id,
    dependent: :destroy
  )

  # => Validations
  validates_presence_of :name
end