FactoryBot.define do
  factory :sponsor do
    name { Faker::Company.name }
    description { Faker::Company.catch_phrase }
  end
end
