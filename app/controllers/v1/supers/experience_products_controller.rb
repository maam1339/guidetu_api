module V1
  module Supers
    class ExperienceProductsController < ApplicationController
      before_action :authenticate!
      before_action :set_experience, only: %i(update destroy)
      before_action :set_product, only: %i(update destroy)

      def update
        @experience.products << @product

        render json: { product: @product }
      end

      def destroy
        @experience.products.destroy @product
      end

      private

      def set_experience
        @experience = Experience.find params.fetch(:experience_id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def set_product
        @product = Product.find params.fetch(:id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end
    end
  end
end