module V1
  module Supers
    class ProductsController < ApplicationController
      before_action :authenticate!
      before_action :set_product, only: %i(update destroy)

      def update
        if @product.update product_params
          render json: { product: @product }
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @product.destroy
      end

      private

      def set_product
        @product = Product.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def product_params
        params.require('product').permit(:title, :description, :price)
      end
    end
  end
end