FactoryBot.define do
  factory :main_account do
    name { Faker::Company.name }
    account_id { nil }
  end
end
