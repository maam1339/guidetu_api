json.call(
  sponsor,
  :id,
  :name,
  :description,
  :created_at,
  :updated_at,
  :products
)

if sponsor.logo.attached?
  json.logo do
    json.partial! 'v1/shared/image', image: sponsor.logo
  end
end

json.images do
  json.array! sponsor.images do |image|
    json.partial! 'v1/shared/image', image: image
  end
end