Rails.application.routes.draw do
  devise_for :super_users

  # => API v1
  namespace :v1, defaults: { format: :json } do

    # => Users authentication
    namespace :users do
      resource :sessions, only: %i(create show destroy)
      resource :confirmations, only: %i(show create)
    end

    # => Supers scope
    namespace :supers do
      resources :suppliers, :sponsors, :dealers
      resources :products, only: %i(update destroy)
      resources :experiences do
        resources(
          :products,
          controller: :experience_products,
          only: %i(update destroy)
        )
      end
    end
  end
end
