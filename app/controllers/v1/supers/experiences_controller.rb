module V1
  module Supers
    class ExperiencesController < ApplicationController
      before_action :authenticate!
      before_action :set_experience, only: %i(show update destroy)

      def index
        @experiences = Experience.all
      end

      def create
        @experience = Experience.new experience_params

        if @experience.save
          render :show, status: :created
        else
          render json: @experience.errors, status: :unprocessable_entity
        end
      end

      def show; end

      def update
        if @experience.update experience_params
          render :show
        else
          render json: @experience.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @experience.destroy
      end

      private

      def set_experience
        @experience = Experience.find params.fetch(:id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def experience_params
        params.require(:experience).permit(:title, :description)
      end
    end
  end
end