class Sponsor < ApplicationRecord
  # => Associations
  has_many :products, as: :entity, dependent: :destroy

  # => Validations
  validates_presence_of :name, :description

  # => Attaches
  has_one_attached :logo
  has_many_attached :images

  # => Nested attributes
  accepts_nested_attributes_for :products
end
