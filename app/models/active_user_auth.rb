class ActiveUserAuth < ApplicationRecord
  #
  self.abstract_class = true
  #
  # => act as for JWT authentication
  #
  acts_as_token_authenticatable
  #
  # => Include default devise modules.
  # Others available are:
  # :timeoutable, :rememberable.
  #
  devise(
    :database_authenticatable,
    :registerable,
    :recoverable,
    :validatable,
    :confirmable,
    :lockable,
    :trackable
  )
  #
  # => increase the failed_attempts counter if the session is not successful
  # and if failed_attempts exceeds the number by configuration, it blocks
  # the account and sends an email with the unlocking instructions.
  #
  def lock_if_attempts_exed
    increment_failed_attempts && save

    if attempts_exceeded?
      lock_access!
    end
  end
  #
  # => check if the parameter failed attempts exceeds the number of attempts
  # to start the session.
  #
  def attempts_exceeded?
    failed_attempts >= self.class.maximum_attempts
  end
  #
  # => Devise Method that checks whether a password is needed or not.
  # Passwords are always required if it's a new record, or if the
  # password or confirmation are being set somewhere.
  #
  # REWRITED: If resourse diferent to owner return 'false'
  #
  # def password_required?
  #   if type.eql?('Owner')
  #     super
  #   else
  #     false
  #   end
  # end
end
