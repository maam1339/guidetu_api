require 'rails_helper'

RSpec.describe Supplier, type: :model do
  let :supplier do
    build(:supplier)
  end

  subject { supplier }

  it { should respond_to(:name) }
  it { should respond_to(:description) }

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
    it { should be_valid }
  end

  context 'associations' do
    it { should have_many(:products).dependent(:destroy) }
  end

  context 'nested attributes' do
     it { should accept_nested_attributes_for(:products) }
  end
end
