json.user do
  json.call(
    @user,
    :id,
    :type,
    :email,
    :first_name,
    :last_name,
    :authentication_token,
    :created_at,
    :updated_at
  )

  if @user.avatar.attached?
    json.avatar do
      json.partial! 'v1/shared/image', image: @user.avatar
    end
  end
  
  unless @user.account.nil?
    json.account do
      json.call(
        @user.account,
        :id,
        :type,
        :name,
        :created_at,
        :updated_at
      )
    end
  end
end