class Experience < ApplicationRecord
  # => Associations
  has_and_belongs_to_many :products

  # => Validations
  validates_presence_of :title, :description
end
