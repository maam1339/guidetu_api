module V1
  module Users
    class ConfirmationsController < ApplicationController
      before_action :authenticable_resource, only: %i(show create)

      def show
        user = resource.confirm_by_token(params[:confirmation_token])

        if user.confirmed?
          render json: { user: user }
        else
          render(
            json: { error: I18n.t('devise.failure.invalid_token') },
            status: :not_acceptable
          )
        end
      end

      def create
        user = resource.send_confirmation_instructions(confirmation_params)

        if user.errors.empty?
          render(
            json: { message: I18n.t('devise.registrations.signed_up_but_unconfirmed') },
            status: :created
          )
        else
          render(
            json: user.errors,
            status: :unprocessable_entity
          )
        end
      end

      private

      def confirmation_params
        params.require(:user).permit(:email)
      end
    end
  end
end