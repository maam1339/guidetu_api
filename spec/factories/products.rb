FactoryBot.define do
  factory :product do
    title { Faker::Commerce.product_name }
    description { Faker::Food.description }
    price { Faker::Commerce.price }
    entity { nil }
  end
end
