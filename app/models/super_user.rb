class SuperUser < ActiveUserAuth
  # => Associations
  belongs_to :account, class_name: 'MainAccount'

  # => Validations
  validates_presence_of :first_name, :last_name, on: :create

  # => Attaches
  has_one_attached :avatar
end