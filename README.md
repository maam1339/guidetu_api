# GUIDETU API

Serves information to different clients.

[![CircleCI](https://circleci.com/bb/maam1339/guidetu_api.svg?style=shield)](https://circleci.com/bb/maam1339/guidetu_api)

## Software Specifications

- Ruby 2.7.0
- Rails 6.0.2.1
- PostgreSQL

## Installation

#### Set Ruby/RoR versions

It is necessary to have installed RVM for this example.

```bash
rvm install 2.7.0
```

```bash
rvm use ruby-2.7.0@rails6.0.2.1 --create
```

```bash
gem install rails -v 6.0.2.1
```

Check ruby/rails versions.

```bash
ruby -v
rails -v
```
#### Clone the repository

It is necessary to have installed Git for this example.

```bash
git clone git@bitbucket.org:kinedu/kinedu_classrooms_web_api.git
```

Then.

```bash
cd kinedu_classrooms_web_api
```

#### Run the application

Run rails migrations.

```bash
rails db:create && rails db:migrate
```

Install rails dependencies.

```bash
bundle install
```

Run rails server.

```bash
rails s
```

## Enjoy!

