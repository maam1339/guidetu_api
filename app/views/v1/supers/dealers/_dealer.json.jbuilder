json.call(
  dealer,
  :id,
  :name,
  :description,
  :created_at,
  :updated_at
)

if dealer.logo.attached?
  json.logo do
    json.partial! 'v1/shared/image', image: dealer.logo
  end
end

json.images do
  json.array! dealer.images do |image|
    json.partial! 'v1/shared/image', image: image
  end
end