FactoryBot.define do
  factory :supplier do
    name { Faker::Company.name }
    description { Faker::Company.catch_phrase }
  end
end
