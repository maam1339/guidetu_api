module V1
  module Supers
    class DealersController < ApplicationController
      before_action :authenticate!
      before_action :set_dealer, only: %i(show update destroy)

      def index
        @dealers = Dealer.all
      end

      def create
        @dealer = Dealer.new dealer_params

        if @dealer.save
          render :show, status: :created
        else
          render json: @dealer.errors, status: :unprocessable_entity
        end
      end

      def show; end

      def update
        if @dealer.update dealer_params
          render :show
        else
          render json: @dealer.errors, status: :unprocessable_entity
        end
      end

      def destroy
        @dealer.destroy
      end

      private

      def set_dealer
        @dealer = Dealer.find params.fetch(:id)
      rescue ActiveRecord::RecordNotFound
        head :not_found
      end

      def dealer_params
        params.require(:dealer).permit(:name, :description, :logo, images: [])
      end
    end
  end
end