class ApplicationController < ActionController::API
  # => Search if the resource (model) exist in the
  # system and creates an instance variable if so.
  #
  # IMPORTANT: This method is only used for
  # authenticate sessions
  #
  def authenticable_resource
    resource = auth_resource.constantize
    self.class.send(:attr_reader, :resource)
    instance_variable_set(:@resource, resource)
  rescue NameError, NoMethodError
    head :bad_request
  end
  #
  # => Search for the type of resource that will be
  # authenticated in the request headers or params
  #
  def auth_resource
    if params.include? :auth_resource
      params.fetch(:auth_resource)
    else
      request.get_header('HTTP_AUTH_RESOURCE')
    end
  end
  #
  # => Devise method that it is overwritten to generate the current_*
  # according to the "single table inherit" patern of rails.
  # Use the type parameter to generate the current_*
  #
  def authenticate!
    user = find_user_by_authentication_token
    invalid_token if user.nil?

    define_current_user(user)
  rescue NameError, NoMethodError
    head :bad_request
  end
  #
  # => *
  #
  def find_user_by_authentication_token
    auth_resource.constantize.find_by(
      authentication_token: request.get_header('HTTP_X_USER_TOKEN')
    )
  end
  #
  # => *
  #
  def invalid_token
    render(
      json: { error: I18n.t('devise.failure.invalid_token') },
      status: :not_acceptable
    )
  end
  #
  # => *
  #
  def define_current_user(user)
    self.class.send(:attr_reader, :current_user)
    instance_variable_set(:@current_user, user)
  end
end
