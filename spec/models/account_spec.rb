require 'rails_helper'

RSpec.describe Account, type: :model do
  let :account do
    build(:account)
  end

  subject { account }

  it { should respond_to(:name) }
  it { should respond_to(:type) }
  it { should respond_to(:account_id) }
end
