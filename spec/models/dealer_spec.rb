require 'rails_helper'

RSpec.describe Dealer, type: :model do
  let :dealer do
    build(:dealer)
  end

  subject { dealer }

  it { should respond_to(:name) }
  it { should respond_to(:description) }

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
    it { should be_valid }
  end
end
